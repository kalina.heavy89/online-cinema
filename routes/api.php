<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SubscriptionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('/auth/register', [AuthController::class, 'register']);

Route::post('/auth/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    /*Route::get('/me', function(Request $request) {
        return auth()->user();
    });*/
    Route::get('/me', [UserController::class, 'getUserData']);

    Route::post('/auth/update', [AuthController::class, 'update']);

    Route::post('/auth/logout', [AuthController::class, 'logout']);

    Route::get('/home', [HomeController::class, 'index']);

    Route::get('/films', [FilmController::class, 'index']);

    Route::get('/subscription-terms', [SubscriptionController::class, 'index']);

    Route::post('/payments', [SubscriptionController::class, 'savePaymentMethod']);

    Route::get('/payment-methods', [SubscriptionController::class, 'getPaymentMethods']);

    Route::post('/remove-payment', [SubscriptionController::class, 'removePaymentMethod']);

    Route::put('/subscription', [SubscriptionController::class, 'updateSubscription']);

});
