import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './pages/Home.vue';
import HomeGuest from './pages/HomeGuest.vue';
import Login from './pages/Login.vue';
import Register from './pages/Register.vue';
import Cabinet from './pages/Cabinet.vue';
import Payment from './pages/Payment.vue';
import Watch from './pages/Watch.vue';
import User from './src/apis/User';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',

    base: process.env.BASE_URL,

    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            props: true
        },
        {
            path: '/guest',
            name: 'home-guest',
            component: HomeGuest
        },
        {
            path: "/login",
            name: "login",
            component: Login,
            meta: { guestOnly: true }
        },
        {
            path: "/register",
            name: "register",
            component: Register,
            meta: { guestOnly: true }
        },
        {
            path: "/cabinet",
            name: "cabinet",
            component: Cabinet,
            meta: { authOnly: true }
        },
        {
            path: "/payment",
            name: "payment",
            component: Payment,
            meta: { authOnly: true }
        },
        {
            path: "/watch",
            name: "watch",
            component: Watch,
            meta: { authOnly: true }
        }
    ]
});

function isLoggedIn() {
    return !!localStorage.getItem("token");
}

/*function isSubscribedIn() {
    if (this.user.access_type == 'subscribed') {
        return true;
    }
    return false;
}*/

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.authOnly)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (!isLoggedIn()) {
        next({
          path: "/login",
          //query: { redirect: to.fullPath }
        });
      } else {
          /*if (!isSubscribedIn()) {
              next({
                  path: "/payment",
              });
          } else {
              next();
          }*/
        next();
      }
    } else if (to.matched.some(record => record.meta.guestOnly)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (isLoggedIn()) {
        next({
            path: "/cabinet",
            //query: { redirect: to.fullPath }
        });
      } else {
        next();
      }
    } else {
      next(); // make sure to always call next()!
    }
});

export default router;
