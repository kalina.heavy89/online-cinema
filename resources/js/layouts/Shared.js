export default {
    data: {
        form: {
            id: "",
            name: "",
            email: "",
            password: "",
            password_confirmation: ""
        },
        check_name: {
            name_required: true,
            name_min_length: true,
            name_max_length: true,
            name_string: true,
            name_special: true,
            name_valid: true
        },
        check_email: {
            email_required: true,
            email_string: true,
            email_email: true,
            email_max_length: true,
            email_special: true,
            email_valid: true
        },
        check_password: {
            password_required: true,
            password_minimum_length: true,
            password_number: true,
            password_lowercase: true,
            password_uppercase: true,
            password_special: true,
            password_confirmation: true,
            password_valid: true
        },
        special_chars: /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/
    },
    methods: {
        checkName() {
            this.check_name.name_required = true;
            this.check_name.name_min_length = true;
            this.check_name.name_max_length = true;
            this.check_name.name_string = true;
            this.check_name.name_special = true;
            this.check_name.name_valid = true;

            if (! this.form.name) {
                this.check_name.name_required = false;
                this.check_name.name_valid = false;
            } else if (this.form.name.length < 3) {
                this.check_name.name_min_length = false;
                this.check_name.name_valid = false;
            } else if (this.form.name.length > 255) {
                this.check_name.name_max_length = false;
                this.check_name.name_valid = false;
            } else if (
                !(/[a-z]/.test(this.form.name)) &&
                !(/[A-Z]/.test(this.form.name))
            ) {
                this.check_name.name_string = false;
                this.check_name.name_valid = false;
            } else if (/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(this.form.name)) {
                this.check_name.name_special = false;
                this.check_name.name_valid = false;
            }

            return this.check_name.name_valid;
        },
        checkEmail() {
            this.check_email.email_required = true;
            this.check_email.email_string = true;
            this.check_email.email_email = true;
            this.check_email.email_max_length = true;
            this.check_email.email_special = true;
            this.check_email.email_valid = true;

            if (! this.form.email) {
                this.check_email.email_required = false;
                this.check_email.email_valid = false;
            } else if (
                !(/[a-z]/.test(this.form.email)) &&
                !(/[A-Z]/.test(this.form.email))
            ) {
                this.check_email.email_string = false;
                this.check_email.email_valid = false;
            } else if (
                !(this.form.email.includes(".", 0)) ||
                !(this.form.email.includes("@", 0))
            ) {
                this.check_email.email_email = false;
                this.check_email.email_valid = false;
            } else if (this.form.email.length > 255) {
                this.check_email.email_max_length = false;
                this.check_email.email_valid = false;
            } else if (/[ `!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/.test(this.form.email)) {
                this.check_email.email_special = false;
                this.check_email.email_valid = false;
            }

            return this.check_email.email_valid;
        },
        checkPassword() {
            this.check_password.password_minimum_length = true;
            this.check_password.password_number = true;
            this.check_password.password_lowercase = true;
            this.check_password.password_uppercase = true;
            this.check_password.password_special = true;
            this.check_password.password_confirmation = true;
            this.check_password.password_valid = true;

            if (! this.form.password) {
                this.check_password.password_required = false;
                this.check_password.password_valid = false;
            } else if (!(this.form.password.length > 5)) {
                this.check_password.password_minimum_length = false;
                this.check_password.password_valid = false;
            } else if (!(/\d/.test(this.form.password))) {
                this.check_password.password_number = false;
                this.check_password.password_valid = false;
            } else if (!/[a-z]/.test(this.form.password)) {
                this.check_password.password_lowercase = false;
                this.check_password.password_valid = false;
            } else if (!/[A-Z]/.test(this.form.password)) {
                this.check_password.password_uppercase = false;
                this.check_password.password_valid = false;
            } else if (/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(this.form.password)) {
                this.check_password.password_special = false;
                this.check_password.password_valid = false;
            } else if (this.form.password !== this.form.password_confirmation) {
                this.check_password.password_confirmation = false;
                this.check_password.password_valid = false;
            }

            return this.check_password.password_valid;
        },
    },
    foo: function() { alert("foo!") }
}

