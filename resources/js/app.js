/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import "bootstrap/dist/css/bootstrap.min.css";

window.Vue = require('vue').default;

import router from './router';
import App from './layouts/App.vue';
import Vue from "vue";
import store from './Store/index';

/*import { StripePlugin } from '@vue-stripe/vue-stripe';

const options = {
    pk: process.env.STRIPE_PUBLISHABLE_KEY,
    stripeAccount: process.env.STRIPE_ACCOUNT,
    apiVersion: process.env.API_VERSION,
    locale: process.env.LOCALE,
};

Vue.use(StripePlugin, options);*/


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('login', require('./pages/Login.vue').default);

//Vue.component('mess', require('./pages/Home.vue'));

Vue.config.productionTip = false;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const app = new Vue({
    router,
    store: store,
    el: '#app',
    render: h => h(App)
});

