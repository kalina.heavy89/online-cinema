import Api from "./Api";

export default {

    async getFilmsData() {
        return Api.get("/films");
      }

}
