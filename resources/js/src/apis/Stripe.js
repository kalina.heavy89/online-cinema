import {loadStripe} from '@stripe/stripe-js';

export default {
    async load() {
      return await loadStripe('pk_test_51IDrwCA88gle3mg5Yf0WYFznwgtl03dfmae5jBTZDg4sDEYC8WqNeaVQ9dSMZ5F4pBZTZcJ1eduYpPdqS8gC2icW00UW2nn4X8');
    },
}
