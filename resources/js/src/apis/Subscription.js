import Api from "./Api";

export default {

    async getSubscriptionTerms() {
        return Api.get("/subscription-terms");
    },

    async savePaymentMethod(method) {
        return Api.post("/payments", {payment_method: method});
    },

    async getPaymentMethods() {
        return Api.get("/payment-methods");
    },

    async removePaymentMethod(paymentID) {
        return Api.post("/remove-payment", {id: paymentID});
    },

    async updateSubscription(plan, payment) {
        return Api.put("/subscription", {
            plan: plan,
            payment: payment
        })
    }

}
