import axios from "axios";

let Api = axios.create({
  baseURL: "/api"
});

Api.defaults.withCredentials = true;

Api.interceptors.request.use(request => {
    if (localStorage.getItem("token")) {
        request.headers['Authorization'] = 'Bearer ' + localStorage.getItem("token");
    }

    return request;
}, error => {
    return Promise.reject(error);
});

export default Api;
