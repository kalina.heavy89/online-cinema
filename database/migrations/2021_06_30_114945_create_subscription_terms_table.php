<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_terms', function (Blueprint $table) {
            $table->id();
            $table->integer('months');
            $table->decimal('monthly_price', $precision=10, $scale=2);
            $table->decimal('total_price', $precision=10, $scale=2);
            $table->char('stripe_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_terms');
    }
}
