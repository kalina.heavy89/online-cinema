<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SubscriptionTerms;
use App\Models\User;
//use App\Models\Subscription;
use Illuminate\Support\Facades\Auth;
//use App\Providers\AppServiceProvider;
//use Illuminate\Database\Eloquent\Model;
use App\Services\UserService;

use Laravel\Cashier\Cashier;

class SubscriptionController extends Controller
{
    public $userService;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get data from subscription_terms table.
     *
     * @param Request
     * @return Response
     */
    public function index()
    {
        $this->user = Auth::user();
        return response()->json([
            'subscription_terms' => SubscriptionTerms::all(),
            'intent' => $this->user->createSetupIntent(),
            'stripe_public_key' => env('STRIPE_KEY')
        ]);
    }

    /**
     * Adds a payment method to the current user.
     *
     * @param Request $request The request data from the user.
    */
    public function savePaymentMethod( Request $request ){
        $this->user = Auth::user();
        $paymentMethodID = $request->get('payment_method');

        if( $this->user->stripe_id == null ){
            $this->user->createAsStripeCustomer();
        }
        $this->user->addPaymentMethod( $paymentMethodID );
        $this->user->updateDefaultPaymentMethod( $paymentMethodID );

        return response()->json( null, 204 );
    }

    /**
     * Returns the payment methods the user has saved
     *
     * @param void
     * @return Response
     */
    public function getPaymentMethods(){
        $user = Auth::user();

        $methods = array();

        if( $user->hasPaymentMethod() ){
            foreach( $user->paymentMethods() as $method ){
                array_push( $methods, [
                    'id' => $method->id,
                    'brand' => $method->card->brand,
                    'last_four' => $method->card->last4,
                    'exp_month' => $method->card->exp_month,
                    'exp_year' => $method->card->exp_year,
                ] );
            }
        }

        return response()->json( $methods );
    }

    /**
     * Removes a payment method for the current user.
     *
     * @param Request $request The request data from the payment ids.
     */
    public function removePaymentMethod( Request $request ){
        $user = Auth::user();
        $paymentMethodID = $request->get('id');

        $paymentMethods = $user->paymentMethods();

        foreach( $paymentMethods as $method ){
            if( $method->id == $paymentMethodID ){
                $method->delete();
                break;
            }
        }

        return response()->json( null, 204 );
    }

    /**
     * Updates a subscription for the user
     *
     * @param Request $request The request containing subscription update info.
     */
    public function updateSubscription( Request $request ){
        $user = Auth::user();
        $plan = $request->get('plan');
        $planID = $plan['stripe_id'];
        $payment = $request->get('payment');
        $paymentID = $payment['id'];

        $subscriptionTerms = SubscriptionTerms::all();
        $subscriptionNames = [];
        foreach ($subscriptionTerms as $subscriptionTerm) {
            if ($subscriptionTerm['months'] > 1) {
                $months = ' months';
            } else {
                $months = ' month';
            }
            $subscriptionNames[] = $subscriptionTerm['months'] . $months;
        }

        /*$subscriptionName = Subscription::where('user_id', $user->id)
            ->orderBy('user_id', 'desc')
            ->first()
            ->name;*/

        $subscribed = false;

        if ($user->access_type == 'subscribed') {
            $subscriptionName = $this->userService->findById($user->id)->subscriptionTerms()->first()->name;
            if ($user->subscribed($subscriptionName)) {
                $user->subscription($subscriptionName)->swap($planID);
                $subscribed = true;
                dump('update subscription');
            }
        }

        if (!$subscribed) {
            $user->newSubscription( $subscriptionNames[$plan['id'] - 1], $planID )->create( $paymentID );
            $subscribed = true;
            dump('new subscription');
        }

        if ($subscribed) {
            $this->userService->changeSubscriptionUserData($plan['id']);
            return response()->json([
                'subscription_updated' => true
            ]);
        }

        return response()->json([
            'subscription_updated' => false
        ]);
    }

}
