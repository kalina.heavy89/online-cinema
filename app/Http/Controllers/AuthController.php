<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserUpdateRequest;

class AuthController extends Controller
{
    use ApiResponser;

    public function register(UserRegisterRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'password' => bcrypt($request['password']),
            'email' => $request['email']
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return $this->success([
                'login' => 'on',
                'token' => auth()->user()->createToken('API Token')->plainTextToken
            ]);
        }

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function login(UserLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return $this->error('Credentials not match', 401);
        }

        return $this->success([
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'Tokens Revoked'
        ]);
    }

    public function update(UserUpdateRequest $request)
    {
        $currentUser = User::find($request->id);

        $currentUser->name = $request['name'];
        if ($request->password) {
            $currentUser->password = bcrypt($request['password']);
        }
        $currentUser->save();
        return $this->success([
            'token' => Auth::user()->createToken('API Token')->plainTextToken
        ]);
    }

}
