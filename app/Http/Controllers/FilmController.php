<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FilmService;

class FilmController extends Controller
{
    private $filmService;

    public function __construct(FilmService $filmService)
    {
        $this->filmService = $filmService;
    }

    public function index()
    {
        return response()->json([
            'films' => $this->filmService->all()
        ]);
    }

}
