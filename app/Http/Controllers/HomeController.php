<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

class HomeController extends Controller
{
    private const SITE_URL = 'https://www.imdb.com/chart/top/?ref_=nv_mv_250';

    private $client;
    private $promise;
    private $siteRequest
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->client = new Client();
        //$this->request = $request = new GuzzleRequest('GET', self::SITE_URL);
    }

    private function getSiteData(string $siteUrl)
    {
        $this->siteRequest = new GuzzleRequest('GET', $siteUrl);

        // Задаем анонимную функцию, которая будет обрабатывать ответ сервера
        $this->promise = $this->client->sendAsync($this->siteRequest)->then(function ($response) {
            echo '<h1>От сервера получен ответ: </h1><br> ' . $response->getBody();
        });

        // Запускаем соединение с удаленным ресурсом и передаем управление по обработке ответа анонимной функции
        $this->promise->wait();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return view('home');
        $this->getSiteData(self::SITE_URL);
        return back()->with('externalSitePage', $this->promise);
    }
}
