<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use App\Models\User;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getUserData()
    {
        /*$this->userService->checkEndSubscriptionPeriod();

        return Auth::user()->with('subscriptionTerms')->first();*/
        return $this->userService->getUserData();
    }

}
