<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Film;

class FilmRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var Film $model
    */
    public function __construct(Film $model)
    {
        $this->model = $model;
    }

    public function clearAll()
    {
        $this->model->truncate();
    }

    public function fillingFilms(array $data)
    {
        $this->model->insert($data);
    }

}
