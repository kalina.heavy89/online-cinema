<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var User $model
    */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Get authenticate user data with relation subscriptionTerms()
     *
     * @return Collection
     */
    public function getUserData()
    {
        return Auth::user()->with('subscriptionTerms')->first();
    }

}
