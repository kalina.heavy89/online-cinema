<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Illuminate\Support\Facades\Storage;
use Sunra\PhpSimple\HtmlDomParser;
//use Goutte\Client;

class GetExternalHtml_draft extends Command
{
    private const SITE_URL = 'https://www.imdb.com/chart/top/?ref_=nv_mv_250';

    private $guzzle;
    private $settings;
    private $siteRequest;
    private $promise;
    private $externalHtml;
    private $dom;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:external-htmll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get external HTML and save needed data into the data base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->guzzle = new GuzzleClient();
        $this->settings['timeout'] = 60;
        //$this->settings['headers'] = [];
        $this->siteRequest = new GuzzleRequest('GET', self::SITE_URL, $this->settings);
        //$this->dom = new HtmlDomParser;
        //$this->dom = new \DOMDocument('1.0', 'UTF-8');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        define('MAX_FILE_SIZE', 6000000);
        dd(HtmlDomParser::file_get_html('https://www.imdb.com/chart/top/?ref_=nv_mv_250')->plaintext);
        $this->promise = $this->guzzle
            ->sendAsync($this->siteRequest)
            ->then(function ($response) {
                $statusCode = $response->getStatusCode();
                //dump($statusCode);
                $body = $response->getBody()->getContents();
                //dump($body);
                return ['Status: success.', $body];
            });

        try {
            $response = $this->promise->wait();
            //dump($response);
            //Storage::disk('local')->put('imdb-chart.txt', $response[1]);
        } catch (\Exception $exception) {
            $response = ['Status: error.', $exception->getMessage()];
        }

        if ($response[0] != 'Status: success.') {
            dd($response[1]);
            return;
        }

        $html = HtmlDomParser::str_get_html($response[1]);

        $tables = $html->find('table');

        dd($tables);
        //$htmlString = Storage::get('imdb-chart.txt');

        $htmlString = str_replace("/&(?!\S+;)/", "&amp;", $htmlString);

        // set error level
        $internalErrors = libxml_use_internal_errors(true);

        //$this->dom->loadHTML($htmlString);

        if (!empty($htmlString)) {
            $html = HtmlDomParser::str_get_html($htmlString);
            dd($html);

            foreach ($html->find('table tr td') as $td) {
                if (!empty($td->a)) {
                    $anchors[] = $td->a;
                }
            }
        }

        // Restore error level
        libxml_use_internal_errors($internalErrors);

        $tables = $this->dom->getElementsByTagName('table')->item(0);
        //$value = $this->dom->documentElement->textContent;
        $trs = [];
        $trsItems = [];
        $imgSrcs = [];
        $trs = $tables->getElementsByTagName('tr');
        for ($i = 0; $i < $tables->getElementsByTagName('tr')->length; $i++) {
            $tr = $trs->item($i);
            $img = $tr->getElementsByTagName('img')->item(0);
            $imgSrcs[] = $img->getAttribute('src');
            //$img = $trChildren->getElementsByTagName('img')->item(0);
            dump($trChildren);

            /*foreach ($trChildren as $trChild) {
                $j = 0;
                $trChild = $trChildren->item($j);
                dump($trChild);
                $img = $trChild->getElementsByTagName('img')->item(0);
                $imgSrcs[] = $img->getAttribute('src');
                $j++;
            }*/
        }

        dd($imgSrcs);

        $trVals = [];
        $img = null;

        $i = 0;
        foreach ($trs as $tr) {
            $trChildren = $tr->childNodes;
            $i = 0;
            foreach ($trChildren as $trChild) {
                $td = $trChild->item($i);
                $img = $tr->getElementsByTagName('img')->item(0);
                $imgSrcs[] = $img->getAttribute('src');
                $i++;
            }
            //$trVals[] = $tr->textContent;
        }
        dd($imgSrcs);


        //dd($this->dom);
        $table = $this->dom->getElementsByTagName('table');
        dd($table);
        $trs = $table->getElementsByTagName('tr');
        $img = null;
        $imgSrcs = [];
        foreach ($trs as $tr) {
            $img = $tr->getElementsByTagName('img');
            $imgSrcs[] = $img->getAttribute('href');
        }
        Storage::disk('local')->put('img-srcs.txt', $imgSrcs);

        /*// Go to the symfony.com website
        $crawler = $this->guzzle->request('GET', self::SITE_URL);
        // Get the latest post in this category and display the titles
        $table = $crawler->filter('tbody > tr');
        dd($table);*/


        return 0;
    }
}
