<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class DateTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:date-test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command to work with dates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $previousDate = '2021-06-29';
        $currentDate = '2021-06-30';
        $nextDate = '2021-07-01';
        $now = Carbon::now()->setTimezone('Europe/Kiev');
        //$nowDate = Carbon::parse($now)->format('Y-m-d');
        $dataTimeString = new Carbon;
        $dateArray = explode("-", $currentDate);
        $dataTimeString->setDateTime($dateArray[0], $dateArray[1], $dateArray[2], 23, 59, 59)->toDateTimeString();
        dump($now);
        dump($dataTimeString);
        dump($now->lessThanOrEqualTo($dataTimeString));
        dump(auth()->user());
        return 0;
    }
}
