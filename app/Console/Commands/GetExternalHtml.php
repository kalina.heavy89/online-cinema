<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\FilmService;

class GetExternalHtml extends Command
{
    private $filmService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:external-html';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get external HTML and save needed data into the data base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FilmService $filmService)
    {
        parent::__construct();
        $this->filmService = $filmService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->filmService->getFilmsData();
    }
}
