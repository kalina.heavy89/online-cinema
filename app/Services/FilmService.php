<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\FilmRepository;
use Goutte\Client;

class FilmService extends BaseService
{
    private const PARENT_URL = 'https://www.metacritic.com';
    private const RATING_URL = '/browse/movies/score/metascore/all/filtered?sort=desc';

    /**
     * Undocumented variable
     *
     * @var FilmRepository $repo
     */
    public $repo;

    /**
     * Constructor.
     *
     * @var FilmRepository $repo
     */
    public function __construct(FilmRepository $repo)
    {
        $this->repo = $repo;
    }

    private function groupDataToBulkDBWrite (array $data)
    {
        $groupedData = [];
        $keys = array_keys($data);
        $sizeData = count($data[$keys[0]]);

        for ($i = 0; $i < $sizeData; $i++) {
            foreach ($keys as $key) {
                $groupedData[$i][$key] = $data[$key][$i];
            }
        }

        return $groupedData;
    }

    public function getFilmsData()
    {
        $client = new Client();
        $crawler = $client->request('GET', self::PARENT_URL . self::RATING_URL);
        $filmData = [];

        $filmData['name'] = $crawler->filter('.clamp-summary-wrap > a > h3')->each(function ($node) {
            return $node->text();
        });
        $filmData['url'] = $crawler->filter('.clamp-image-wrap > a')->each(function ($node){
            return $node->attr('href');
        });
        foreach ($filmData['url'] as &$url) {
            $url = self::PARENT_URL . $url;
        }
        unset($url);
        $filmData['img_src'] = $crawler->filter('.clamp-image-wrap > a > img')->each(function ($node){
            return $node->attr('src');
        });

        $filmData['release_date'] = $crawler
            ->filter('.clamp-details > span:not(.cert_rating)')
            ->each(function ($node) {
                return $node->text();
            });
        $filmData['description'] = $crawler->filter('.summary')->each(function ($node) {
            return $node->text();
        });

        $filmData['score'] = $crawler
            ->filter('.clamp-summary-wrap > .clamp-score-wrap > a > div')
            ->each(function ($node){
                return $node->text();
            });

        $groupFilmsData = $this->groupDataToBulkDBWrite($filmData);

        $this->repo->clearAll();
        $this->repo->fillingFilms($groupFilmsData);

    }

}
