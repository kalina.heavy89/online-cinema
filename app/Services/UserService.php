<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\SubscriptionTerms;

class UserService extends BaseService
{
    /**
     * Undocumented variable
     *
     * @var UserRepository $repo
     */
    public $repo;

    /**
     * Constructor.
     *
     * @var UserRepository $repo
     */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Check date `end_subscr_period` from `users` table
     *
     * @param void
     * @return void
     */
    public function checkEndSubscriptionPeriod()
    {
        $user = Auth::user();
        if ($user->access_type == 'subscribed') {
            $endSubscriptionPeriod = $user->end_subscr_period;
            $now = Carbon::now()->setTimezone('Europe/Kiev');
            $dataTimeString = new Carbon;
            $dateArray = explode("-", $endSubscriptionPeriod);

            $dataTimeString->setDateTime($dateArray[0], $dateArray[1], $dateArray[2], 23, 59, 59)->toDateTimeString();

            if ($now->lessThanOrEqualTo($dataTimeString)) {
                return;
            }
        }

        $this->repo->update($user->id, ['access_type' => 'free']);
    }

    public function getUserData()
    {
        $this->checkEndSubscriptionPeriod();

        return $this->repo->getUserData();
    }

    /**
     * Change subscription user data
     *
     * @param int $subscriptionId
     * @return void
     */
    public function changeSubscriptionUserData(int $subscriptionId): void
    {
        $changeUserData = [];
        $user = Auth::user();
        $subscriptionData = SubscriptionTerms::where('id', $subscriptionId)->first();
        $changeUserData['access_type'] = 'subscribed';
        $changeUserData['beg_subscr_period'] = Carbon::now()->setTimezone('Europe/Kiev')->format('Y-m-d');
        $changeUserData['end_subscr_period'] = Carbon::now()
            ->setTimezone('Europe/Kiev')
            ->add($subscriptionData->months, 'month')
            ->format('Y-m-d');
        $changeUserData['subscription_id'] = $subscriptionId;
        $this->update($user->id, $changeUserData);
    }
}
