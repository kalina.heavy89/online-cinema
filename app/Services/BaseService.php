<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Repositories\BaseRepository;

abstract class BaseService
{
    public $repo;

    /**
    * Constructor
    *
    * @var BaseRepository $repo
    */
    public function __construct(BaseRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Get all data
    *
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->repo->all();
    }

    /**
    * Create new record
    *
    * @param array $input
    * @return model
    */
    public function create(array $data): Model
    {
        return $this->repo->create($data);
    }

    /**
    * Update data
    *
    * @param integer $id
    * @param array $data
    * @return boolean
    */
    public function update(string $id, array $data)
    {
        return $this->repo->update($id, $data);
    }

    /**
    * Delete record by id
    *
    * @param integer $id
    * @return boolean
    */
    public function destroy(string $id): bool
    {
        return $this->repo->destroy($id);
    }

    /**
    * Find record by id
    *
    * @param int $id
    * @return Model
    */
    public function findById(int $id): Model
    {
        return $this->repo->findById($id);
    }

    /**
    * Show the first record from the database
    *
    * @return model
    */
    public function first()
    {
        return $this->repo->first();
    }

    /**
    * Show the latest record from the database
    *
    * @return model
    */
    public function last()
    {
        return $this->repo->last();
    }

}
