<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'category'
    ];

    /**
     * Get the movies for the category.
     *
     * @return collection
     */
    public function movies()
    {
        return $this->hasMany(Movie::class, 'category_id', 'id');
    }
}
